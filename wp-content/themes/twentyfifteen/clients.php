
<?php

/*
 Template Name: Clients
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>


<link rel="stylesheet" href="assets/css/normalize.css">
<link rel="stylesheet" href="assets/css/layout.css">
<script type="text/javascript" src="assets/js/jquery.mixitup.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
<script src="assets/js/modernizr.custom.97074.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="assets/css/noJS.css"/></noscript>
<script type="text/javascript" src="assets/js/jquery.hoverdir.js"></script>

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$args = array( 'numberposts' =>-1, 'post_type'=>'event_clients', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$nav_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

// $title = get_the_title();
// $nav_id = get_the_ID();
$image = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );

$category = get_field('category', $nav_id);
$category = $category[0];
$category = $category -> post_name;

$nav_items[] = array('image'=> $image, 'category'=> $category);

endforeach;
wp_reset_postdata();


$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}


$args = array( 'numberposts' =>-1, 'post_type'=>'client_cat', 'suppress_filters' => 0, 'order' => 'ASC');
$client_cat = get_posts( $args );
$client_cat_items = array();

foreach ( $client_cat as $post ) :   setup_postdata( $post );

$title = get_the_title();
$slug = $post -> post_name;
// print_r($slug);die;

$client_cat_items[] = array('title'=> $title, 'slug'=> $slug);

endforeach;
wp_reset_postdata();


$all_cat_string = "";

$counter = 0;
$max = count($client_cat_items);

foreach ($client_cat_items as $key => $value) {
  
  $all_cat_string = $all_cat_string.".".$value['slug'];
  $counter++;

  if($counter < $max)
  {
    $all_cat_string = $all_cat_string.", ";
  }
}

// print_r($client_cat_items);die;

?>

<link href="assets/css/clients.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    <div class="title_span_div">
      <span class="title_span">Our Clients</span>
    </div>
    <ul id="filters" class="clearfix">
        <li><span class="filter all_filters" data-filter="<?= $all_cat_string; ?>">Show All</span></li>
        <?php
        foreach ($client_cat_items as $key => $value) {
          ?>
          <li><span class="filter" data-filter=".<?= $value['slug']; ?>"><?= $value['title']; ?></span></li>
          <?php
        }
        ?>
      </ul>
    <div class="client_list_div da-thumbs container" id="portfoliolist">
      <?php

      foreach ($nav_items as $key => $value) {
        
        ?>
        <div class="single_client_div portfolio <?= $value['category'];?>" data-cat="<?= $value['category'];?>">
          <img src="<?= $value['image']; ?>">
        </div>
        <?php
      }

      ?>

    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>
  
   <?php 
  include 'footer_custom.php';
  ?>
  
</div>
<?php get_footer(); ?>
<script>

$( document ).ready(function() {

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


    });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));

  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');


    $('.nav_bar').addClass('nav_bar_slide');

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

 var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$(function () {
    
    var filterList = {
    
      init: function () {
      
        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            // filter: '.all_filters'  
          }     
        });               
      
      }

    };
    
    // Run the show!
    filterList.init();
    $('.all_filters').addClass('active');
    
    
  }); 

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);

});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>