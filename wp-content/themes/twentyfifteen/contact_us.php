
<?php

/*
 Template Name: Contact Us
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$location = get_field('location', get_the_ID());

$phone = get_field('phone', get_the_ID());
$phone_2 = str_replace("(", "", $phone);
$phone_2 = str_replace(")", "", $phone_2);
$phone_2 = str_replace("-", "", $phone_2);
$phone_2 = str_replace(" ", "", $phone_2);

$email = get_field('email', get_the_ID());

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

?>


<link href="assets/css/contact_us.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    <div class="title_span_div">
      <span class="title_span">Contact Us</span>
    </div>
    <div class="client_list_div">
      <div class="loc_div con_divs">
        <div class="cont_custom">
          <img src="assets/img/cont_icons/loc_icon_idle.png" class="idle_imgs">
          <img src="assets/img/cont_icons/loc_icon_active.png" class="active_imgs">
        </div>
        <span><?= $location; ?></span>
      </div>
      <div class="phone_div con_divs">
        <a href="tel:<?= $phone_2; ?>">
          <div class="cont_custom">
            <img src="assets/img/cont_icons/phone_icon_idle.png" class="idle_imgs">
            <img src="assets/img/cont_icons/phone_icon_active.png" class="active_imgs">
          </div>
        </a>
        <span><?= $phone; ?></span>
      </div>
      <div class="email_div con_divs">
        <a href="mailto:<?= $email; ?>" target="_top">
          <div class="cont_custom">
            <img src="assets/img/cont_icons/email_icon_idle.png" class="idle_imgs">
            <img src="assets/img/cont_icons/email_icon_active.png" class="active_imgs">
          </div>
        </a>
        <span><?= $email; ?></span>
      </div>
    </div>
  </div>
  <div id="map" class="maps">
      
    </div>
  
   <?php 
  include 'footer_custom.php';
  ?>
  
</div>
<?php get_footer(); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0qC4hqI1v6LMLdZjQJaSbnK-_0XlgY24"></script>

<script>

var map1, positions;
var markers = {};

function initialize()
  {

      var map_items = $('.map_items');
      var focus_lat =  30.0866929;
      var focus_lng =  31.3254074;
      var title = "Event House";

      var mapCanvas1 = document.getElementById('map');
      var mapOptions1 = {
        center: new google.maps.LatLng(focus_lat,focus_lng),
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        streetViewControl: false,
        mapTypeControl: true,
        scrollwheel: false,
        zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_TOP
          }
      }

      map1 = new google.maps.Map(mapCanvas1, mapOptions1);

      positions = {lat: focus_lat, lng: focus_lng};
      var markers = new google.maps.Marker({
        position: positions,
        map: map1,
        title: title
      });
  }

$( document ).ready(function() {

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


    });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));

  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');


    $('.nav_bar').addClass('nav_bar_slide');

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 initialize();
 $('#map').append('<div class="arrow-up-right_footer"></div><div class="arrow-up-left_footer"></div>');

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

 var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

initialize();
$('#map').append('<div class="arrow-up-right_footer"></div><div class="arrow-up-left_footer"></div>');

var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);

 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>