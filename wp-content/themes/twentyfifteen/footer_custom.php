<div class="footer">
    <div class="footer_div_1">
      <span>follow us on..</span>
      <div class="footer_img_div">
        <a href="https://www.youtube.com/channel/UCUqvyV22cJZzfk8WFPOKiyw" target="_blank" class="footer_href">
          <img class="idle_imgs" src="assets/img/sm_icons/yt_idle.png">
          <img class="active_imgs" src="assets/img/sm_icons/yt_hover.png">
        </a>
        <a href="https://www.facebook.com/EventHouseEgypt" target="_blank" class="footer_href">
          <img class="idle_imgs" src="assets/img/sm_icons/fb_idle.png">
          <img class="active_imgs" src="assets/img/sm_icons/fb_hover.png">
        </a>
        <a href="https://www.instagram.com/eventhouseegypt/?hl=en" target="_blank" class="footer_href">
          <img class="idle_imgs" src="assets/img/sm_icons/insta_idle.png">
          <img class="active_imgs" src="assets/img/sm_icons/insta_hover.png">
        </a>
      </div>
    </div>
    <div class="footer_copy_div">
      <span>&copy; 2017 EVENT HOUSE. All Rights Reserved. Developed & Designed by Hug Digital</span>
    </div>
  </div>