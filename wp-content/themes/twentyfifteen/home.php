
<?php

/*
 Template Name: Homepage
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<link rel="stylesheet" href="assets/css/font-awesome.css">
<link href="assets/css/hover.css" rel="stylesheet" media="screen">
<!-- <script src="assets/js/responsive-nav.js"></script> -->
<script src="assets/js/jquery.popupoverlay.js"></script>


<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;


global $post;
$page_id =  $post->ID;

$args = array( 'numberposts' =>-1, 'post_type'=>'event_clients', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$clients_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

// $title = get_the_title();
// $nav_id = get_the_ID();
$image = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );

$clients_items[] = $image;

endforeach;
wp_reset_postdata();

$portfolio = get_field('portfolio', get_the_ID());

$who_we_are = get_field('who_we_are', get_the_ID());
$who_we_are = strip_tags($who_we_are, '<p>');
$who_we_are = strip_tags($who_we_are, '<span>');

// print_r($who_we_are_right);die;

$args = array( 'numberposts' =>-1, 'post_type'=>'testimonials', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$test_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

$author = get_field('author', get_the_ID());
$title = get_field('title', get_the_ID());
$testimonial = get_field('testimonial', get_the_ID());

$test_items[] = array('author'=> $author, 'title'=> $title, 'testimonial'=> $testimonial);
// print_r($test_items);die;

endforeach;
wp_reset_postdata();

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}


$args = array( 'numberposts' =>-1, 'post_type'=>'events', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$events_items = array();


foreach ( $navs as $post ) :   setup_postdata( $post );

$event_id = get_the_ID();
$title = get_the_title();
$thumb = wp_get_attachment_url( get_post_thumbnail_id($event_id) );

$desc = get_field('desc', get_the_ID());
$date = get_field('date', get_the_ID());

$date = DateTime::createFromFormat('dmY', $date);

$day =  $date->format('d');
$week_day =  $date->format('D');
$month =  $date->format('M');

$excerpt = strip_shortcodes($desc);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, 135);
$excerpt = $excerpt . '...';

$dateNumber = strtotime($date->format('Y').'-'.$date->format('m').'-'.$date->format('d'));


$currentDate = date("Y-m-d");
$currentTime = date("H:i:s");
$now = strtotime($currentDate . $currentTime);

// print_r($now);die;
// $month_day = date("j",$date);
// $month = date("M",$date);

if($dateNumber > $now)
{
  $events_items[$dateNumber] = array('title' => $title, 'thumb' => $thumb, 'desc' => $desc, 'excerpt' => $excerpt, 'day' => $day, 'week_day' => $week_day, 'month' => $month);
}

endforeach;
wp_reset_postdata();

ksort($events_items);

// print_r($events_items);die;

?>


<link href="assets/css/home_new.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>

  <div class="fixed_div">
    <a href="https://www.facebook.com/TheMarqueeOfficial/" target="_blank" class="hvr-pulse">
      <img src="assets/img/marq_side.png">
    </a>
  </div>
  <div class="main_div_2">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>

    <div class="top_div">
      <?php
    echo do_shortcode('[rev_slider alias="home"]');
    ?>
      <!-- <img src="assets/img/watch_vid.png"> -->
    </div>

    <div class="client_list_div">
      <div class="rect_div"></div>
      <div class="arrow-up-right-top"></div>
      <div class="arrow-up-left-top"></div>
      <div class="title_div_1">
        <span class="title_div_1_span_1">this is</span>
        <span class="title_div_1_span_2">Who We Are</span>
      </div>
      <div class="who_we_are_div container">
        <p><?= $who_we_are; ?></p>
      </div>
    </div>
    <div class="journey_div">
      <div class="arrow-up-right-mid"></div>
      <div class="arrow-up-left-mid"></div>
        <div class="journey_title_div">
          <span class="journey_title_div_span_1">A continuous</span>
          <span class="journey_title_div_span_2">Success Journey</span>
        </div>
        <div class="all_clients_list">
          <div id="owl-demo" class="owl-carousel owl-theme owl-demo-2">
          <?php 
          foreach ($clients_items as $key => $value) {
            ?>
            <div class="item client_owl">
              <img src="<?= $value;?>">
            </div>
            <?php
          }
          ?>
        </div>
          
        </div>
        <div class="through_div">
          <span class="through_div_span_1">through</span>
        </div>
        <div class="logos_div">
          <div class="logos_div_inner logos_div_inner_upper logo_inner_1">
            <img src="assets/img/icons/corp_icon.png">
            <span>corporate</span>
            <span>events</span>
          </div>
          <div class="logos_div_inner logos_div_inner_upper logo_inner_2">
            <img src="assets/img/icons/road_icon.png">
            <span>roadshows</span>
            <span>& activations</span>
          </div>
          <div class="logos_div_inner logos_div_inner_upper logo_inner_3">
            <img src="assets/img/icons/exhibit_icon.png">
            <span>exhibitions</span>
            <span>& concerts</span>
          </div>
        </div>
        <div class="and_more_div">
          <span class="and_more_div_span_1">and more</span>
        </div>
    </div>
    <div class="client_list_div client_list_div_2">
      <div class="title_div_2">
        <span class="title_div_2_span_1">What</span>
        <span class="title_div_2_span_2">They Said</span>
      </div>
      <div class="test_div">
        <div id="owl-demo" class="owl-carousel owl-theme owl-demo">
          <?php 
          foreach ($test_items as $key => $value) {
            ?>
            <div class="item">
              <p>
                "<?= $value['testimonial']; ?>"
              </p>
              <div class="test_div_bot">
                <span>
                  - <?= $value['author']; ?>
                </span>
                <span>
                  <?= $value['title']; ?>
                </span>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
    <div class="events_div">
      <div class="title_div_2">
        <span class="title_div_2_span_1">upcoming</span>
        <span class="title_div_2_span_2 event_span_2">Events</span>
      </div>
      <div class="events_main_div">
        <?php
        foreach ($events_items as $key => $value) {
          ?>
          <div class="event_inner container">
            <div class="col-sm-1 event_inner_1">
                <div class="event_inner_1_2">
                  <span class="span_1"><?= $value['week_day'] ;?></span>
                  <span class="span_2"><?= $value['day'] ;?></span>
                  <span class="span_3"><?= $value['month'] ;?></span>
                </div>
            </div>
            <div class="col-sm-4 event_inner_2" style="background:url(<?= $value['thumb']; ?>)">
            </div>
            <div class="col-sm-6 event_inner_3">
              <span><?= $value['title']; ?></span>
              <p><?= $value['excerpt']; ?></p>
            </div>
            <a class="initialism event_<?= $key; ?>_open" href="#event_<?= $key; ?>">
              <div class="col-sm-1 event_inner_4">
                <i class="fa fa-plus" aria-hidden="true"></i>
                  <!-- <img src="assets/img/event_plus.png" class="event_arrow idle_imgs">
                  <img src="assets/img/event_plus_hover.png" class="event_arrow active_imgs"> -->
              </div>
            </a>
          </div>

          <div class="event_inner_mobile container">
            <div class="col-sm-4 event_inner_2" style="background:url(<?= $value['thumb']; ?>)">
            </div>
            <div class="col-sm-1 event_inner_1">
                <div class="event_inner_1_2">
                  <span class="span_1"><?= $value['week_day'] ;?></span>
                  <span class="span_2"><?= $value['day'] ;?></span>
                  <span class="span_3"><?= $value['month'] ;?></span>
                </div>
            </div>
            <div class="col-sm-6 event_inner_3">
              <span><?= $value['title']; ?></span>
              <p><?= $value['excerpt']; ?></p>
              <a class="mobile_pop_link initialism event_<?= $key; ?>_open" href="#event_<?= $key; ?>">
                Read More
              </a>
            </div>
          </div>

          <div id="event_<?= $key; ?>" class="well pop_ups">
            <div class="pop_bg" style="background:url(<?= $value['thumb']; ?>)"></div>
            <span class="pop_title"><?= $value['title']; ?></span>
            <p class="pop_desc"><?= $value['desc']; ?></p>
            <button class="event_<?= $key; ?>_close btn btn-default close_btn_new">X</button>
          </div>

          <?php
        }
        ?>

      </div>
    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>

  <?php 
  include 'footer_custom.php';
  ?>

</div>
<?php get_footer(); ?>


<script>

$( document ).ready(function() {

  $('.pop_ups').popup({
        pagecontainer: '.events_main_div',
        transition: 'all 0.3s'
    });

  $('.event_arrow').on('click',function (e) {

    //   $('#fadeandscale').popup({
    //     pagecontainer: '.events_main_div',
    //     transition: 'all 0.3s'
    // });
  
  });

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


  });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));

  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');


    $('.nav_bar').addClass('nav_bar_slide');

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 
 $('.arrow-up-right-top').css('border-right-width', main_width);
 $('.arrow-up-left-top').css('border-left-width', main_width);

 $('.arrow-up-right-mid').css('border-right-width', main_width);
 $('.arrow-up-left-mid').css('border-left-width', main_width);



 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');


 $('.event_inner_1').css('height', $('.event_inner_2').css('height'));
 $('.event_inner_3').css('height', $('.event_inner_2').css('height'));
 $('.event_inner_4').css('height', $('.event_inner_2').css('height'));


 var owl = $(".owl-demo");
 
  owl.owlCarousel({
     
     // items : 5,
      items :1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : false,
      pagination: true,
      touchDrag: true,
      autoplay: true,
      autoplayHoverPause: false
 
  });

  owl.trigger('owl.play',10000);



  owl = $(".owl-demo-2");
 
  owl.owlCarousel({
     
     // items : 5,
      items :6, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,4], // betweem 900px and 601px
      itemsTablet: [600,3], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : false,
      pagination: false,
      touchDrag: true,
      autoplay: true,
      autoplayHoverPause: false
 
  });

  owl.trigger('owl.play',2500);

  var windowWidth = $(window).width();

 if(windowWidth > 767)
{
  var newHeight = parseInt($('.nav_bar').css('height'));
  $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

$('#loading').fadeOut();

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

$('.arrow-up-right-top').css('border-right-width', main_width);
 $('.arrow-up-left-top').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);

 $('.event_inner_1').css('height', $('.event_inner_2').css('height'));
 $('.event_inner_3').css('height', $('.event_inner_2').css('height'));


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>