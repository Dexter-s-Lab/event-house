
<button class="md-trigger md_trigger_form" data-modal="modal-2"></button>
<button class="md-trigger md_trigger_sending" data-modal="modal-3"></button>
<button class="md-trigger md_trigger_success" data-modal="modal-4"></button>
<button class="md-trigger more_info_mod" data-modal="modal-5"></button>


    <div class="md-modal md-effect-1" id="modal-2">
      <div class="md-content">
        <form class="form-horizontal cont_form act_form" formID="4">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input inputID="1" type="text" class="form-control form_inputs" id="inputEmail3" name="name" required>
            </div>
          </div>  
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input inputID="2" type="email" class="form-control form_inputs" id="inputEmail3" name="email" required>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
            <div class="col-sm-10 mob_input mob_input_3">
              <input type="number" class="form-control form_inputs" id="inputEmail3" name="number" required>
            </div>
          </div>
          <input inputID="3" type="hidden" class="custom_number form_inputs">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">I Am..</label>
              <div class="col-sm-10">
                <select inputID="4" name="stat" class="form-control form_select form_inputs" required>
                  <option value = "">Select Status</option>
                  <option value = "Interested in a New Emaar Misr Home" >Interested in a New Emaar Misr Home</option>
                  <option value = "An owner of an Emaar Misr Home">An owner of an Emaar Misr Home</option>
                  <option value = "A resident in an Emaar Misr Home">A resident in an Emaar Misr Home</option>
                </select>
              </div>
            </div>  
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
              <button type="submit" class="btn btn-default submit_btn">Submit</button>
            </div>
          </div>
        </form>
        <div>
          <button class="md-close md-close_2">X</button>
        </div>
      </div>
    </div>
    <div class="md-modal md-effect-1" id="modal-5">
      <div class="md-content">
        <span class="more_title"></span>
        <img src="" class="more_img">
        <div class="more_text">
        </div>
        <div>
          <button class="md-close md-close_2">X</button>
        </div>
      </div>
    </div>

    <div class="md-modal md-effect-2" id="modal-3">
      <div class="md-content">
        <h3><img src="assets/img/spinner.gif"/>Sending</h3>
      </div>
      <button class="md-close"></button>
    </div>


    <div class="md-modal md-effect-22" id="modal-4">
      <div class="md-content">
        <h3>Request Sent!</h3>
      </div>
      <button class="md-close"></button>
    </div>

    <div class="md-overlay"></div>

    <link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>