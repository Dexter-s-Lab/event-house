
<button class="md-trigger md_trigger_form" data-modal="modal-1"></button>
<button class="md-trigger md_trigger_form_2" data-modal="modal-2"></button>
<button class="md-trigger md_trigger_sending" data-modal="modal-3"></button>
<button class="md-trigger md_trigger_success" data-modal="modal-4"></button>
<button class="md-trigger event_more" data-modal="modal-7"></button>

<div class="md-modal md-effect-1" id="modal-1">
      <div class="md-content">
        <span>Select Date</span>
          <select class="selectpicker">
          </select>
        <div class="mod_cont">

        </div>
        <div>
          <button class="md-close">X</button>
        </div>
      </div>
    </div>

    <div class="md-modal md-effect-1" id="modal-2">
      <div class="md-content">
        <div class="mod_cont_2">

        </div>
        <div>
          <button class="md-close md-close_2">X</button>
        </div>
      </div>
    </div>

    <div class="md-modal md-effect-2" id="modal-3">
      <div class="md-content">
        <h3><img src="assets/img/spinner.gif"/>Sending</h3>
      </div>
      <button class="md-close"></button>
    </div>


    <div class="md-modal md-effect-22" id="modal-4">
      <div class="md-content">
        <h3>Request Sent!</h3>
      </div>
      <button class="md-close"></button>
    </div>

    <div class="md-modal md-effect-1" id="modal-7">
      <div class="md-content">
        <div class="eve_modal_img">
          <div class="eve_modal_spans">
            <span class="eve_modal_span_day"></span>
            <span class="eve_modal_span_month"></span>
          </div>
          <img src="">
        </div>
        <span class="eve_modal_span"></span>
        <p class="eve_modal_text"></p>
        <div>
          <button class="md-close md-close_2">X</button>
        </div>
      </div>
    </div>

    <div class="md-overlay"></div>

    <link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>