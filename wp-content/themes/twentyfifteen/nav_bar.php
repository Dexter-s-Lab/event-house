<?php
global $post;
$curr_title = get_the_title();

$custom_post = $post->post_type;
// print_r($custom_post);die;
?>

<div class="overlay" id="loading">
  <div class="sk-circle">
  <div class="sk-circle1 sk-child"></div>
  <div class="sk-circle2 sk-child"></div>
  <div class="sk-circle3 sk-child"></div>
  <div class="sk-circle4 sk-child"></div>
  <div class="sk-circle5 sk-child"></div>
  <div class="sk-circle6 sk-child"></div>
  <div class="sk-circle7 sk-child"></div>
  <div class="sk-circle8 sk-child"></div>
  <div class="sk-circle9 sk-child"></div>
  <div class="sk-circle10 sk-child"></div>
  <div class="sk-circle11 sk-child"></div>
  <div class="sk-circle12 sk-child"></div>
</div>
</div>

<div class="nav_bar">
    <div id="navbar_2" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="logo_nav"><a href="<?php echo get_site_url(); ?>"><img class="event_logo" src="assets/img/event_logo.png"></a></li>
            <?php 

            foreach ($menu as $key => $value) {

              $class = '';

              // if($value['title'] == $curr_title)
              if($value['title'] == $curr_title || ($value['title'] == 'Portfolio' && $custom_post == 'events'))
              $class = "active_menu";

              ?>
              <li class="menu_items <?= $class;?>">
                <a href="<?= $value['url'];?>"><?= $value['title'];?></a>
                <img class="nav_arrow" src="assets/img/nav_arrow_red.png">
              </li>
              <?php
            }
            ?>
          </ul>
        </div>
  </div>
<div class="fake_head">
  </div>
  <nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links" href="#home" >
              <img src="assets/img/event_logo.png">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <?php 

                foreach ($menu as $key => $value) {

                  $class = "";

                  if($value['title'] == $curr_title || ($value['title'] == 'Portfolio' && $custom_post == 'events'))
                  $class = "active_nav";
                  ?>
                  <li class="<?= $class; ?>"><a href="<?= $value['url'];?>" class="mobile_links"><?= $value['title'];?></a></li>
                  <?php
                }
                ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>