<?php
$args = array( 'numberposts' =>-1, 'post_type'=>'social_media', 'suppress_filters' => 0);
$sm = get_posts( $args );
$social_media = array();
foreach ($sm as $key => $value) {
  
  $social_media[$value -> post_title] = $value -> post_content;
}
?>
<div class="footer_div container">
        <div class="col-sm-3 footer_left">
          <img class="footer_img footer_img_1" src="assets/img/footer_logo_1.png"/>
        </div>
        <div class="col-sm-6 footer_mid">
          <span>All rights reserved. Emaar Misr <?php echo date("Y"); ?></span>
          <div>
            <a href="<?= $social_media['Facebook']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/fb_icon.png"/>
            </a>
            <a href="<?= $social_media['Twitter']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/tw_icon.png"/>
            </a>
            <a href="<?= $social_media['Instagram']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/insta_icon.png"/>
            </a>
            <a href="<?= $social_media['Linkedin']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/link_icon.png"/>
            </a>
          </div>
        </div>
        <div class="col-sm-3 footer_right">
          <img class="footer_img footer_img_2" src="assets/img/footer_logo_2.png"/>
        </div>
      </div>