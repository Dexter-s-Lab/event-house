
<?php

/*
 Template Name: Guidelines
 */

?>
<title>Community Services</title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<?php include 'spinner.php'; ?>
<!-- <script src="assets/js/pace.min.js"></script> -->
<script data-pace-options='{ "ajax": false }' src='assets/js/pace.js'></script>

<script src="assets/js/jquery.dimensions.min.js"></script>
<script src="assets/js/jquery.viewport.mini.js"></script>
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;
$page_cont = get_field('content', $page_id);
$curr_order = get_field('order', $page_id);
// print_r($page_cont);

/****************** Nav Items ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'nav_items', 'suppress_filters' => 0);
$navs = get_posts( $args );


foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$nav_id = get_the_ID();
$nav_icon = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );
$order = get_field('order', get_the_ID());
$link = get_field('link', get_the_ID());

$nav_items[$order] = array('title'=> $title, 'icon'=> $nav_icon, 'link'=> $link);

endforeach;
wp_reset_postdata();

ksort($nav_items);
// print_r($nav_items);die;

/**********************************************/

/****************** Guidelines ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'guidelines', 'suppress_filters' => 0);
$guidelines = get_posts( $args );


foreach ( $guidelines as $post ) :   setup_postdata( $post );

$title = get_the_title();
$guide_id = get_the_ID();
$guide_icon = wp_get_attachment_url( get_post_thumbnail_id($guide_id) );
$link = get_permalink(get_the_ID());
$order = get_field('order', get_the_ID());
$content = get_field('content', get_the_ID());

$files_array = array();
$files = get_field('community_files', get_the_ID());

if (is_array($files)) 
{
  foreach ($files as $key => $value) {
    
    $file_id = $value -> ID;
    $file = get_field('file', $file_id);
    $file_title = get_the_title($file_id);
    $files_array[] = array('title'=> $file_title, 'file'=> $file);

    // print_r($file);die;
  }
}

$guide_items[$order] = array('id' => get_the_ID(), 'title'=> $title, 'content'=> $content, 'icon'=> $guide_icon, 'files'=> $files_array
  , 'link'=> $link);

endforeach;
wp_reset_postdata();

ksort($guide_items);
// print_r($guide_items);die;

/**********************************************/


?>


<link href="assets/css/guide.css" rel="stylesheet" media="screen">

<?php include 'mobile_nav.php' ?>

<div class="md-overlay"></div>
  <div class="main_container" id="home">
    <div class="fake_nav"></div>
    <div class="top_div">
      <img src="assets/img/slide_3.jpg">
      </div>

      <?php include 'nav_bar.php' ?>
      <?php include 'modal_event.php' ?>

      <div class="white_strip">
      </div>

      <div class="blue_strip strips">
        <span>Community Services</span>
      </div>

      <div class="what_div container">
        <div class="page_main">
          <?php
          if(!empty($page_cont))
          {
          ?>
          <div class="page_cont">
            <?= $page_cont; ?>
          </div>
          <?php 
          } 
          ?>
          <div class="new_icon_div">
            <?php
            
            $count = 0;

            foreach ($guide_items as $key => $value) {

              $active = '';
              if($value['id'] == $page_id)
              $active = 'eve_active';

              $first = '';
              if($count == 0)
              $first = 'eve_div_first';

              $last = '';
              if($count == count($guide_items) - 1)
              $last = 'eve_div_last';
              
              $file_flag = 'file_flag';
              if(empty($value['files']))
              $file_flag = '';

              ?>
              <div class="eve_2 <?= $active; ?>" eveId="<?= $count; ?>">
                <img src="<?= $value['icon']; ?>">
                <span><?= $value['title']; ?></span>
              </div>
              <div class="hidden_options hidden_<?= $count; ?>">
                <div class="hidden_text">
                  <?= $value['content']; ?>
                </div>
                <div class="hidden_files">
                  <?php
                    // print_r($value['files']);
                    foreach ($value['files'] as $key2 => $value2) {
                      
                      $title = $value2['title'];
                      $file = $value2['file'];

                      ?>
                      <div class="eve_span_div">
                        <img src="assets/img/right_arrow.png">
                        <?php
                        if(!empty($value2['file']))
                        {
                          ?>
                          <a href="<?= $value2['file']; ?>" target="_blank"><span><?= $title; ?></span></a>
                          <?php
                        }
                        else
                        {
                          ?>
                          <span><?= $title; ?></span>
                          <?php
                        }
                        ?>
                        
                      </div>
                      <?php
                    }
                    ?>
                </div>
              </div>
              <?php

              $count++;

            }
            ?>
          </div>
          <div class="main_cont_div" id="main_cont">

          </div>
          <div class="page_icons">
            <?php 

              $count = 0;

              foreach ($guide_items as $key => $value) {
                
                $active = '';
                if($value['id'] == $page_id)
                $active = 'guide_active';

                $first = '';
                if($count == 0)
                $first = 'eve_div_first';

                $last = '';
                if($count == count($guide_items) - 1)
                $last = 'eve_div_last';
                
                $file_flag = 'file_flag';
                if(empty($value['files']))
                $file_flag = '';
                
                  
                ?>
                <div class="eve_div <?= $first; ?> <?= $last; ?> <?= $active; ?>" eveId="<?= $count; ?>">
                  <div class="eve_2 <?= $file_flag; ?>" eveId="<?= $count; ?>">
                    <img src="<?= $value['icon']; ?>">
                    <span><?= $value['title']; ?></span>
                  </div>
                  <div class="eve_files event_<?= $count; ?>">
                    <?php
                    // print_r($value['files']);
                    foreach ($value['files'] as $key2 => $value2) {
                      
                      $title = $value2['title'];
                      $file = $value2['file'];

                      ?>
                      <div class="eve_span_div">
                        <img src="assets/img/right_arrow.png">
                        <?php
                        if(!empty($value2['file']))
                        {
                          ?>
                          <a href="<?= $value2['file']; ?>" target="_blank"><span><?= $title; ?></span></a>
                          <?php
                        }
                        else
                        {
                          ?>
                          <span><?= $title; ?></span>
                          <?php
                        }
                        ?>
                        
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                </div>
                <?php

                $count++;
              }
              ?>
          </div>
        </div>
      </div>

      <div class="white_strip">
      </div>

      <?php include 'new_footer.php' ?>

  </div>  
<?php get_footer(); ?>
<script>

$( document ).ready(function() {

  var eveId = $('.eve_active').attr('eveId');

  var hidden_text = $('.hidden_' + eveId + ' .hidden_text').html();
  var hidden_files = $('.hidden_' + eveId + ' .hidden_files').html();

  $('.main_cont_div').html(hidden_text + hidden_files);


  $('.eve_2').on('click',function (e) {
      
      var eveId = $(this).attr('eveId');

      var hidden_text = $('.hidden_' + eveId + ' .hidden_text').html();
      var hidden_files = $('.hidden_' + eveId + ' .hidden_files').html();

      $('.main_cont_div').fadeOut('');

      setTimeout(function(){ 
        $('.main_cont_div').html(hidden_text + hidden_files);
        $('.main_cont_div').fadeIn(); 

      }, 250);
      
      $('.eve_active').removeClass('eve_active');
      $(this).addClass('eve_active');

    });

  // $('.item').on('click',function (e) {
      
  //     var itemCount = $(this).attr('itemCount');

  //     $('.active_cont').fadeOut('');
  //     $('.active_cont').removeClass('active_cont');
      
  //     $('.item_cont_' + itemCount).fadeIn('');
  //     $('.item_cont_' + itemCount).addClass('active_cont');

      
  //     $('.active_item').removeClass('active_item');
  //     $('.item_' + itemCount).addClass('active_item');


  //   });

});

window.onload = function() {

 
  var nav_height = $('.mobile_nav').css('height');
  $('.fake_nav').css('height', nav_height);

  $('.logo_div').fadeOut();
  $('.main_container').css('opacity', 1);

  $('.guide_active .eve_files').slideDown();

  var x = $('#main_cont').offset().top;

  $('html, body').stop().animate({
      'scrollTop': x
  }, 900, 'swing', function () {
      // window.location.hash = target;
  });

}
</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>