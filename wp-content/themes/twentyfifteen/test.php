
<?php

/*
 Template Name: Events
 */

?>
<title>Events</title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<script src="assets/js/responsive-nav.js"></script>

<?php include 'spinner.php'; ?>

<script data-pace-options='{ "ajax": false }' src='assets/js/pace.js'></script>

<script src="assets/js/jquery.dimensions.min.js"></script>
<script src="assets/js/jquery.viewport.mini.js"></script>
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;
$page_cont = get_field('content', $page_id);
$curr_order = get_field('order', $page_id);
// print_r($page_cont);

$current_url = $_SERVER['REQUEST_URI'];
$current_url = explode('/', $current_url);
$current_url = $current_url[count($current_url) - 2];
$books_offset = intval($current_url);

if(empty($books_offset))
$books_offset = 1;

$books_array = array();

$current_page = $books_offset;
$books_offset = ($books_offset - 1) * 6;  

// print_r($books_offset);die;

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = array( 'numberposts' => -1, 'post_type'=>'banners', 'suppress_filters' => 0);
$banners = get_posts( $args );
foreach ( $banners as $post ) :   setup_postdata( $post );

$slug = $post->post_name;

if($slug == 'events')
{
  $banner = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );
}

endforeach;
wp_reset_postdata();

/****************** Nav Items ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'nav_items', 'suppress_filters' => 0);
$navs = get_posts( $args );


foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$nav_id = get_the_ID();
$nav_icon = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );
$order = get_field('order', get_the_ID());
$link = get_field('link', get_the_ID());

$nav_items[$order] = array('title'=> $title, 'icon'=> $nav_icon, 'link'=> $link);

endforeach;
wp_reset_postdata();

ksort($nav_items);
// print_r($nav_items);die;

/**********************************************/

/****************** Events ******************/

$count = 0;
$count_2 = 0;

// $args = array( 'numberposts' =>50,'offset' =>$books_offset, 'post_type'=>'events', 'suppress_filters' => 0, 'paged' => $paged,
//   'orderby' => 'date','order' => 'DESC');
$args = array( 'numberposts' =>-1, 'post_type'=>'events', 'suppress_filters' => 0, 'paged' => $paged,
  'orderby' => 'date','order' => 'DESC');
$events = get_posts( $args );

foreach ( $events as $post ) :   setup_postdata( $post );

// if($count_2 == 6)
// break;

$title = get_the_title();
$port_image = get_field('portrait_image', get_the_ID());
$land_image = get_field('landscape_image', get_the_ID());

$date = get_field('date', get_the_ID());
$date = DateTime::createFromFormat('dmY', $date);
$dateNumber = strtotime($date->format('Y').'-'.$date->format('m').'-'.$date->format('d'));

$day =  $date->format('d');
$month =  $date->format('M');

$link = get_permalink();

$gallery = get_field('gallery', get_the_ID());
$pics_array = array();

if(!empty($gallery))
{
  

  $gallery = $gallery[0]['ngg_id'];
  $pictures = $nggdb->get_gallery($gallery , 'sortorder', 'ASC', true, 0, 0);

  foreach($pictures as $image) {

      if($image->pid == $image->previewpic)
      $cover = $image->imageURL;

      $pics_array[] = $image->imageURL;
   }

   if(!empty($pics_array))
   {
	   $events_array[$dateNumber + $count] = array('title'=> $title, 'land_image'=> $land_image, 'day'=> $day, 'month'=> $month, 'link'=> $link);
	   $events_array[$dateNumber + $count]['cover'] = $cover;
	   $events_array[$dateNumber + $count]['pics'] = $pics_array;


	   if(!isset($events_array[$dateNumber + $count]['cover']))
		$events_array[$dateNumber + $count]['cover'] = $land_image;

	   $count_2++;
   }
   
}

$count++;

endforeach;
wp_reset_postdata();

krsort($events_array);

$temp_array = $events_array;
$temp_array_2 = array();
$events_array = array();

foreach ($temp_array as $key => $value) {
	
	$temp_array_2[] = $value;
}


$books_all = $count_2;
$max_pages = ceil($books_all/6);

$start_offset = $books_offset;
$end_offset = $current_page * 6;

// print_r($start_offset.'--'.$end_offset);
// print_r($temp_array_2);


for ($i=$start_offset; $i < $end_offset ; $i++) { 
  
  $events_array[] = $temp_array_2[$i];

}

// print_r($temp_array_2);

// $events_array_temp = $events_array;
// $events_array = array();

// $new_count = 0;
// foreach ($events_array_temp as $key => $value) {
  
//   if($key == 6)
//   break;
    
//   $events_array[] = $value;
//   $new_count
// }

// print_r($events_array);die;
/**********************************************/


?>


<link href="assets/css/events.css" rel="stylesheet" media="screen">

<?php include 'analyticstracking.php' ?>
<?php include 'mobile_nav.php' ?>

<div class="md-overlay"></div>
  <div class="main_container" id="home">
    <div class="fake_nav"></div>
    <div class="top_div">
      <img src="<?= $banner; ?>">
      </div>

      <?php include 'nav_bar.php' ?>
      <?php include 'modal_event.php' ?>

      <div class="white_strip">
      </div>

      <div class="blue_strip strips">
        <span>Events</span>
      </div>

      <div class="what_div container">
        <div class="gallery_row container">
        <?php

        $new_count = 0;

        foreach ($events_array as $key => $value) {

          $last = '';
          if($key ==6)
          $last = 'last_div';

          $empty = '';
          if(!isset($value['pics']))
          $empty = 'gallery_empty';
            
          ?>
          <?php
          if(empty($empty))
          {
            ?>
            <a href="<?= $value['link'];?>">
            <?php
          }
          ?>
            <div class="gallery_cols col-sm-3 <?= $last; ?> <?= $empty; ?>" style="background-image: url(<?= $value['cover'] ?>)">       
              <div class="gallery_background">
              </div>
              <div class="gallery_inside_div" style="width: 100%;">
                <div class="event_date_span">
                  <span class="span_1"><?= $value['day'] ;?></span>
                  <span class="span_2"><?= $value['month'] ;?></span>
                </div>
                <div style="float:left;width: 70%;height: 50px;display: table;">
                <span class="g_spans gallery_workshop_title" style="display: table-cell;vertical-align: middle;"><?= $value['title'] ?></span>
              </div>
              </div>
            </div>
            <?php
            if(empty($empty))
            {
              ?>
              </a>
              <?php
            }
            ?>

          <?php

          // $new_count++;
          // if($new_count == 6)
          // break;  
        }
        ?>
        </div>

        <?php

      $prev_url = get_site_url().'/events/'.($current_page - 1);
      $next_url = get_site_url().'/events/'.($current_page + 1);

      if($current_page != $max_pages)
      $next = '';
      else  
      $next = 'no_link'; 

      if($current_page != 1)
      $prev = '';
      else  
      $prev = 'no_link'; 

      ?>
      <div class="page_div">
        <a href="<?= $prev_url; ?>">
          <div class="links prev_link <?= $prev?>">
            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
          </div>
        </a>
        <?php

        $start = ceil($current_page/5);

        if($start != 1)
        $start = (($start - 1) * 5) + 1;

        $end = $start + 5;

        for ($i = $start; $i < $end; $i++) { 
          
          $active = '';

          if($current_page == $i)
          $active = 'links_active';

          if($current_page == $max_pages)
          $next = 'no_link';

          $current_url = get_site_url().'/events/'.$i;
          ?>
          <a href="<?= $current_url; ?>" class="<?= $active; ?>" >
            <div class="links">
              <span><?= $i?></span>
            </div>
          </a>
        <?php

        if($i == $max_pages)  
        break;

        }
        

        ?>
        <a href="<?= $next_url; ?>">
          <div class="links next_link <?= $next?>">
            <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
          </div>
        </a>
      </div>
      </div>

      <div class="white_strip">
      </div>

      <?php include 'new_footer.php' ?>

  </div>  
<?php get_footer(); ?>
<script>

$( document ).ready(function() {

  $('.eve_span_div').on('click',function (e) {
      
      
      var fileFlag = $(this).attr('fileFlag');

      if(fileFlag == 0)
      return false;
        
      var file = $(this).find('.hidden_options').html();
      $('.eve_modal').html(file);

      $('.md_trigger_form').trigger('click');
      $('.md-close').fadeIn(1800);

      // console.log(file);

    });

  $('.eve_2').on('click',function (e) {
      
      var eveId = $(this).attr('eveId');
      var old_id = $('.guide_active').attr('eveId');

      if(eveId == old_id)
      {
        $('.guide_active').removeClass('guide_active');
        $('.event_' + eveId).slideUp();
      }

      else
      {
        $('.eve_files').slideUp();
        $('.guide_active').removeClass('guide_active');
        $(this).parent().addClass('guide_active');
        $('.event_' + eveId).slideDown();
      }

    });

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


    });

  $('.selectpicker').on('change', function () {
      
      var fID = $(this).find('option:selected').attr('fID');
      $('.pdf_div').hide();
      $('.pdf_div_'+fID).show();

    });

});

window.onload = function() {

  var nav_height = $('.mobile_nav').css('height');
  $('.fake_nav').css('height', nav_height);

  $('.logo_div').fadeOut();
  $('.main_container').css('opacity', 1);

  $('.guide_active .eve_files').slideDown();

}
</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>