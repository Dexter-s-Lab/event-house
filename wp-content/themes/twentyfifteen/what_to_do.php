
<?php

/*
 Template Name: What To Do
 */

?>
<title>What to do in Marassi</title>

<?php
get_header(); 

/****************** Nav Items ******************/

$args = array( 'numberposts' =>-1, 'post_type'=>'nav_items', 'suppress_filters' => 0);
$navs = get_posts( $args );


foreach ( $navs as $post ) :   setup_postdata( $post );

$title = get_the_title();
$nav_id = get_the_ID();
$nav_icon = wp_get_attachment_url( get_post_thumbnail_id($nav_id) );
$order = get_field('order', get_the_ID());

$nav_items[$order] = array('title'=> $title, 'icon'=> $nav_icon);

endforeach;
wp_reset_postdata();

ksort($nav_items);
// print_r($nav_items);die;

/**********************************************/

?>

<link href="assets/css/single_activity.css" rel="stylesheet" media="screen">

<div class="md-overlay"></div>
  <div class="main_container" id="home">
    <div class="top_div">
      <img src="assets/img/slide_2.jpg">
      </div>
      <div class="nav_bar_main">
        <div class="nav_bar">
          <?php
          foreach ($nav_items as $key => $value) {
            ?>
            <div class="nav_item">
              <img src="<?= $value['icon']; ?>">
              <span><?= $value['title']; ?></span>
            </div>
            <?php
          }
          ?>
        </div>
      </div>

      <div class="white_strip">
      </div>

      <div class="orange_strip">
        <span>What to do in Marassi</span>
      </div>

      <div class="what_div container">
        <div class="what_slider">
              <div id="owl-demo" class="owl-carousel owl-theme owl-demo">
                <?php 
                query_posts('post_type=activities&order=ASC&offset=0');

                      while ( have_posts() ) : the_post();

                        $title = get_the_title();
                        $logo = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
                        $link = get_permalink(get_the_ID());
                        ?>
                        <a href="<?= $link; ?>">
                          <div class="item">
                            <img src="<?= $logo?>"/>
                            <span><?= $title; ?></span>
                          </div>
                        </a>
                        <?php
                      endwhile;
                      wp_reset_query();
                      ?>
              </div>
            </div>  
      </div>

      <div class="footer_strip">
      </div>

      <div class="footer_bg">
        <img src="assets/img/footer_bg.jpg">
      </div>

      <div class="white_strip">
      </div>

      <div class="footer_div container">
        <div class="col-sm-3">
          <img class="footer_img footer_img_1" src="assets/img/footer_logo_1.png"/>
        </div>
        <div class="col-sm-6 footer_mid">
          <span>All rights reserved. Emaar Misr <?php echo date("Y"); ?></span>
          <div>
            <a href="<?= $social_media['Facebook']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/fb_icon.png"/>
            </a>
            <a href="<?= $social_media['Twitter']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/tw_icon.png"/>
            </a>
            <a href="<?= $social_media['Instagram']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/insta_icon.png"/>
            </a>
            <a href="<?= $social_media['Linkedin']; ?>" target="_blank">
              <img class="footer_icons" src="assets/img/link_icon.png"/>
            </a>
          </div>
        </div>
        <div class="col-sm-3 footer_right">
          <img class="footer_img footer_img_2" src="assets/img/footer_logo_2.png"/>
        </div>
      </div>
  </div>
    
<?php get_footer(); ?>

<script>

$( document ).ready(function() {

var owl = $(".owl-demo");
 
  owl.owlCarousel({
     
     // items : 5,
      items :4, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      scrollPerPage : true
 
  });


});

window.onload = function() {

 

  $('#loading').fadeOut(1500); 

}
</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>